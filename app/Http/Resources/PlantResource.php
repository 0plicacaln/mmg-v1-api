<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlantResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'cultivar' =>  $this->whenNotNull($this->cultivar),
            'description' =>  $this->whenNotNull($this->description),
            'plant_definition' => new PlantDefinitionResource($this->plant_definition),
            'zone' => new ZoneResource($this->whenNotNull($this->zone)),
            'tasks' => TaskResource::collection($this->whenLoaded('tasks')),
        ];
    }
}
